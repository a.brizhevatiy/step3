// import { ModalWindow } from "./classes/modals.js";

import {VisitCardTherapist, VisitCardCardiologist, VisitCardDentist} from "./classes/cards.js";
import BdWalker from "./classes/BdWalker.js";
import {getToken, signOut, signIn, getLogin, createNewCard, showCards} from "./functions/functions.js";
import {
    NewInput,
    NewSelect,
    NewTextArea,
    VisitDoctor,
    VisitCardiologist,
    VisitDentist,
    VisitTherapist,
    ChangeVisitDoctor,
    Form,
    FormLogin,
    FormVisitCreate,
    FormChangeCard
} from "./classes/Modal.js";

const container = document.querySelector(".cards_desk");
const walker = new BdWalker();
let formObj = {};

const newVisit = document.querySelector("#newVisit_button");
newVisit.addEventListener("click", () => {
    let divFon = document.createElement("div");
    divFon.classList.add("divFon");
    let body = document.querySelector("body");
    body.append(divFon);
    let divModal = document.querySelector(".modal");
    let form = new FormVisitCreate("modalEl");
    if (divModal !== null) {
        if (confirm("Карточка не сохранена, удалить введенные данные?")) {
            divModal.remove();
            form.createForm();
        }
    } else {
        form.createForm();
    }
});

const loginBtn = document.querySelector("#login_button");
loginBtn.addEventListener("click", () => {
    let body = document.querySelector("body");
    let divFon = document.createElement("div");
    divFon.classList.add("divFon");
    body.append(divFon);
    let form = new FormLogin("modalEl");
    form.createForm();
});

const logoutBtn = document.querySelector("#logout_button");
logoutBtn.addEventListener("click", () => {
    loginBtn.style.display = "block";
    newVisit.style.display = "none";
    logoutBtn.style.display = "none";
    signOut();
    //удаляется блок с карточками
});


const cred = { // для тестов, в дальнейшем эти данные заираются из полей логина и пароля
    login: "al@ukr.net",
    pass: '12345'
};

if (getToken()) {
    loginBtn.style.display = "none";
    newVisit.style.display = "block";
    logoutBtn.style.display = "block";
    showCards(); // повесить на авторизацию, после как она прошла успешно
}



