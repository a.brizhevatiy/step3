import BdWalker from "../classes/BdWalker.js";
import {VisitCardTherapist, VisitCardCardiologist, VisitCardDentist} from '../classes/Cards.js';

const walker = new BdWalker();

function getToken() { // получить токен из куки
    return getCookie('token');
}

function getLogin() { // получить логин из куки
    return getCookie('login');
}


function getCookie(name) { //функция для получения значения куки по имени
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

async function signIn(user) { // получить из БД токен  и если успешно - записать токен и логин в куки
    const data = await walker.getToken({
        'email': user.login,
        'password': user.pass
    });
    if (data.status !== "Success") {
        console.error(data)
        return data;
    } else {
        document.cookie = `token=${data.token}; path=/; max-age=7200`; // Сохраняю токены в Куки на 2 часа, при повторном вызове время обновляется
        document.cookie = `login=${user.login}; path=/; max-age=7200`;
        showCards();
        return data;
    }
}

function signOut() { // очистить куки
    document.cookie = `token=''; path=/; max-age=-1`;
    document.cookie = `login=''; path=/; max-age=-1`;
    document.querySelector('.cards_desk').innerHTML = '<p class="main__text">No items have been added</p>';
    return getCookie('token') === undefined ? 1 : -1;
}

async function createNewCard(data) { //повесить на кнопку добавления карты
    await walker.addCard(data);
    await showCards();
    return data;
}

async function showCards(filter) { // получает данные с сервера и создает на основании их карты,
    // для фильтрации - передать объект с полями для фильтра
    const cards_desk = document.querySelector('.cards_desk'); // Общий див для карточек
    const token = getToken();
    let cards = await walker.getCard({token: token}); // массив карточек с сервера
    if(cards.length === 0){ // если вернулся пустой массив
        cards_desk.innerHTML = '<p class="main__text">No items have been added</p>';
        return;
    }
    if (filter) { // при наличии объекта фильтра
        cards = cards.filter(card => {
            for (const item in filter) {
                if (filter[item] !== card[item]) { // если значение поля фильтра не совпадает с полем карточки - ретерним
                    return false; //
                }
            }
            return true; // если поля совпали
        })
    }
    cards_desk.innerHTML = '';
    let data = null;
    if (cards.status === 'Error') {
        console.error(cards.message);
        cards_desk.innerHTML = '<p class="main__text">No items have been added</p>';// если карточки не добавились
    } else { // в зависимости от врача - создаем нужный объект
        for (const card of cards) {
            if (card.doctor === 'Терапевт') {
                data = new VisitCardTherapist(card);
            } else if (card.doctor === 'Кардиолог') {
                data = new VisitCardCardiologist(card);
            } else if (card.doctor === 'Стоматолог') {
                data = new VisitCardDentist(card);
            } else {
                console.error("Ошибка создания карточки из объекта");
                return;
            }
            data.createCard(); // Создаем карточку
            data.render(); // Добавляем на страницу
        }
        if (cards_desk.innerHTML === '') {
            cards_desk.innerHTML = '<p class="main__text">No items have been added</p>';// если карточки не добавились
        }
    }
}

export {getToken, signOut, signIn, getLogin, createNewCard, showCards}